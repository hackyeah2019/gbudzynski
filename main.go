package main

import (
	"fmt"
	awsTools "gbudzynski/aws"
	comprehendTools "gbudzynski/aws/comprehend"
	dbTools "gbudzynski/db"
	"gbudzynski/google/nlp"
	"gbudzynski/handlers"
	"net/http"
	"os"
)

func main() {
	db := dbTools.MustNew()
	defer db.Close()

	session := awsTools.MustNewSession()
	comprehendClient := comprehendTools.NewClient(session)
	nlpClient := nlp.MustNewClient()

	http.Handle("/", &handlers.TestHandler{DB: db})
	http.Handle("/messages", &handlers.Messages{
		ComprehendClient: comprehendClient,
		NLPClient:        nlpClient,
	})
	err := http.ListenAndServe(fmt.Sprintf(":%s", PORT), nil)
	if err != nil {
		panic(err)
	}
}

var (
	PORT = os.Getenv("PORT")
)
