package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

func MustNewSession() *session.Session {
	return session.Must(session.NewSession(&aws.Config{
		Region: aws.String(AWS_REGION),
	}))
}

const (
	AWS_REGION = "eu-central-1"
)
