package comprehend

import (
	. "gbudzynski/nlp"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/comprehend"
	"strings"
)

func NewClient(s *session.Session) *ComprehendClient {
	return &ComprehendClient{
		comprehendClient: comprehend.New(s),
	}
}

func (c *ComprehendClient) MustDetectLanguage(str string) string {
	res, err := c.comprehendClient.DetectDominantLanguage(&comprehend.DetectDominantLanguageInput{
		Text: &str,
	})
	if err != nil {
		panic(err)
	}
	return *res.Languages[0].LanguageCode
}

func (c *ComprehendClient) MustDetectEntities(lang, str string) []*Entity {
	output, err := c.comprehendClient.DetectEntities(&comprehend.DetectEntitiesInput{
		LanguageCode: &lang,
		Text:         &str,
	})
	if err != nil {
		panic(err)
	}

	entities := output.Entities
	retEntities := make([]*Entity, len(entities))
	for index, entity := range entities {
		retEntities[index] = &Entity{
			Type: *entity.Type,
			Text: strings.ToLower(*entity.Text),
		}
	}
	return retEntities
}

type ComprehendClient struct {
	comprehendClient *comprehend.Comprehend
}
