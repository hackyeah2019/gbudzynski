package leon

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"
)

func PostMessage(message *Message) error {
	serialized, err := json.MarshalIndent(message, "", "  ")
	if err != nil {
		return err
	}

	fmt.Printf("%s\n", serialized)

	req, err := http.NewRequest("POST", LEON_APP+"/broker/message", bytes.NewBuffer(serialized))
	if err != nil {
		return err
	}
	req.Header.Set("content-type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	res.Body.Close()
	return nil
}

func MustGetAirports() *GetAirportsResponse {
	req, err := http.NewRequest("GET", LEON_APP+"/lot/airports", nil)
	if err != nil {
		panic(err)
	}

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	resObj := &GetAirportsResponse{}
	json.NewDecoder(res.Body).Decode(resObj)
	return resObj
}

func MustGetAvail(departureDate, returnDate time.Time, iata, city string) *GetAvailRetObject {
	req, err := http.NewRequest("GET", LEON_APP+"/lot/hotplace", nil)
	if err != nil {
		panic(err)
	}

	dateFormat := "02012006"

	q := req.URL.Query()
	q.Set("departureDate", departureDate.Format(dateFormat))
	q.Set("returnDate", returnDate.Format(dateFormat))
	q.Set("destination", iata)
	req.URL.RawQuery = q.Encode()

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	resObj := &GetAvailResponse{}
	json.NewDecoder(res.Body).Decode(resObj)

	if len(*resObj) == 0 {
		return nil
	} else if len((*resObj)[0]) == 0 {
		return nil
	} else if len((*resObj)[0][0]) == 0 {
		return nil
	}
	obj := (*resObj)[0][0][0]
	segments := obj.Outbound.Segments
	if len(segments) == 0 {
		return nil
	}
	segment := segments[len(segments)-1]

	return &GetAvailRetObject{
		Price:       obj.Outbound.Price,
		URL:         obj.URL,
		ArrivalDate: segment.ArrivalDate,
		City:        strings.Title(city),
	}
}

type GetAvailResponse []GetAvail

type GetAvail []GetAvailArray

type GetAvailArray []GetAvailObject

type GetAvailObject struct {
	Outbound OutboundType `json:"outbound"`
	URL      string       `json:"url"`
}

type GetAvailRet []*GetAvailRetObject

type GetAvailRetObject struct {
	Price       float64 `json:"price"`
	URL         string  `json:"url"`
	ArrivalDate string  `json:"arrivalDate"`
	City        string  `json:"city"`
}

type OutboundType struct {
	Price    float64   `json:"price"`
	Segments []Segment `json:"segments"`
}

type Segment struct {
	ArrivalDate string `json:"arrivalDate"`
}

type GetAirportsResponse []City

type City struct {
	Name    string `json:"city"`
	IATA    string `json:"iata"`
	Country string `json:"country"`
}

type Message struct {
	Type      MessageType   `json:"type"`
	Status    MessageStatus `json:"status"`
	Text      string        `json:"text"`
	To        int           `json:"to"`
	Timestamp int           `json:"timestamp"`
}

type MessageType string

const (
	Text    MessageType = "text"
	Weather             = "weather"
	Flights             = "flights"
)

type MessageStatus string

const (
	Success MessageStatus = "success"
	Error                 = "error"
)

var (
	client   = &http.Client{}
	LEON_APP = os.Getenv("LEON_APP")
)
