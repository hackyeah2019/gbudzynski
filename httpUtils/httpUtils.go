package httpUtils

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func AsJSON(w http.ResponseWriter) {
	w.Header().Set("content-type", "application/json")
}

func MeasureExecutionTime(r *http.Request, start time.Time) {
	end := time.Now()
	fmt.Printf("%s %s %v\n", r.Method, r.URL.Path, end.Sub(start))
}

func CatchPanic(w http.ResponseWriter) {
	err := recover()
	if err == nil {
		return
	}
	code := http.StatusInternalServerError
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(map[string]interface{}{
		"code":    code,
		"message": http.StatusText(code),
	})
	fmt.Printf("%v\n", err)
}
