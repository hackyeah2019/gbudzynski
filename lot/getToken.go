package lot

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func mustGetToken() string {
	body := &GetTokenRequest{
		SecretKey: LOT_SECRET,
		Params: GetTokenRequestParams{
			Market:   "PL",
			Language: "EN",
		},
	}

	serialized, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest("POST", baseURL+"/auth/token/get", bytes.NewBuffer(serialized))
	if err != nil {
		panic(err)
	}

	req.Header.Set("content-type", "application/json")
	setAPIKey(req)

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	resObj := &GetTokenResponse{}
	err = json.NewDecoder(res.Body).Decode(resObj)
	if err != nil {
		panic(err)
	}
	return resObj.Token
}

type GetTokenRequest struct {
	SecretKey string                `json:"secret_key"`
	Params    GetTokenRequestParams `json:"params"`
}

type GetTokenRequestParams struct {
	Market   string `json:"market"`
	Language string `json:"language"`
}

type GetTokenResponse struct {
	Token string `json:"access_token"`
}
