package lot

import (
	"fmt"
	"net/http"
	"os"
)

func MustNewClient() *Client {
	client := &Client{
		token: mustGetToken(),
	}
	go client.refreshToken()
	return client
}

type Client struct {
	token string
}

func setAPIKey(r *http.Request) {
	r.Header.Set("x-api-key", LOT_KEY)
}

func (c *Client) setAuthorization(r *http.Request) {
	r.Header.Set("authorization", fmt.Sprintf("Bearer %s", c.token))
}

const (
	baseURL = "https://api.lot.com/flights-dev/v2"
)

var (
	client     = &http.Client{}
	LOT_KEY    = os.Getenv("LOT_KEY")
	LOT_SECRET = os.Getenv("LOT_SECRET")
)
