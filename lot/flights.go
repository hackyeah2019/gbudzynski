package lot

import (
	"fmt"
	"gbudzynski/leon"
	"github.com/araddon/dateparse"
	"strings"
	"time"
)

func (c *Client) MustGetFlights(date, location string) *leon.GetAvailRetObject {
	airports := c.mustGetAirports()
	iata := getIATAByLocation(location, airports)
	if iata == "" {
		panic(fmt.Errorf("could not determine IATA for location %q", location))
	}

	departureDate, returnDate := tryParse(date)
	return leon.MustGetAvail(departureDate, returnDate, iata, location)
}

func tryParse(date string) (time.Time, time.Time) {
	date = strings.ToLower(date)
	dateObj, err := dateparse.ParseAny(date)
	if err == nil {
		return dateObj.Add(-DAY), dateObj.Add(DAY)
	}

	dateObj, err = time.Parse("January", strings.Title(date))
	if err == nil {
		return time.Date(time.Now().Year(), dateObj.Month(), 0, 0, 0, 0, 0, time.Local),
			time.Date(time.Now().Year(), dateObj.Month(), 30, 0, 0, 0, 0, time.Local)
	}

	if date == "tomorrow" {
		return time.Date(time.Now().Year(), time.Now().Month(), time.Now().Add(DAY).Day(), 0, 0, 0, 0, time.Local),
			time.Date(time.Now().Year(), time.Now().Month(), time.Now().Add(DAY).Day(), 23, 59, 59, 0, time.Local)
	}

	if date == "next week" {
		return time.Now(), time.Now().Add(WEEK)
	}

	if date == "next month" {
		return time.Now(), time.Now().Add(MONTH)
	}

	panic("could not determine time using any method")
}

func (c *Client) mustGetAirports() *leon.GetAirportsResponse {
	return leon.MustGetAirports()
}

func getIATAByLocation(location string, airports *leon.GetAirportsResponse) string {
	for _, city := range *airports {
		if strings.Contains(strings.ToLower(city.Name), strings.ToLower(location)) {
			return city.IATA
		}
	}
	return ""
}

const (
	DAY   = 24 * time.Hour
	WEEK  = 7 * DAY
	MONTH = 30 * DAY
	YEAR  = 365 * DAY
)
