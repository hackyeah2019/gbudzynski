package lot

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func (c *Client) refreshToken() {
	for range time.Tick(time.Second * 10) {
		// c.mustRefreshToken()
		c.token = mustGetToken()
	}
}

// TODO: refreshing token seems not to work on LOT side.
// It returns new token in response.
func (c *Client) mustRefreshToken() {
	req, err := http.NewRequest("POST", baseURL+"/auth/token/refresh", nil)
	if err != nil {
		panic(err)
	}

	req.Header.Set("content-type", "application/json")
	setAPIKey(req)
	c.setAuthorization(req)

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	io.Copy(os.Stdout, res.Body)
	fmt.Printf("\n%s\n", fmt.Sprintf("Bearer %s", c.token))
}
