package nlp

import (
	"fmt"
)

func (e Entity) String() string {
	return fmt.Sprintf("%s (%s)", e.Text, e.Type)
}

type Entity struct {
	Type string
	Text string
}
