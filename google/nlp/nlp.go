package nlp

import (
	language "cloud.google.com/go/language/apiv1"
	"context"
	. "gbudzynski/nlp"
	"google.golang.org/api/option"
	proto "google.golang.org/genproto/googleapis/cloud/language/v1"
	"os"
	"strings"
)

func MustNewClient() *Client {
	ctx := context.Background()

	client, err := language.NewClient(ctx, option.WithCredentialsJSON([]byte(os.Getenv("GOOGLE_APPLICATION_CREDENTIALS"))))
	if err != nil {
		panic(err)
	}
	return &Client{
		ctx:    ctx,
		client: client,
	}
}

func (n *Client) MustDetectEntities(str string) []*Entity {
	res, err := n.client.AnalyzeEntities(n.ctx, &proto.AnalyzeEntitiesRequest{
		Document: &proto.Document{
			Source: &proto.Document_Content{
				Content: str,
			},
			Type: proto.Document_PLAIN_TEXT,
		},
		EncodingType: proto.EncodingType_UTF8,
	})

	if err != nil {
		panic(err)
	}

	entities := res.Entities
	retEntities := make([]*Entity, len(entities))
	for index, entity := range entities {
		retEntities[index] = &Entity{
			Type: entity.Type.String(),
			Text: strings.ToLower(entity.Name),
		}
	}

	return retEntities
}

type Client struct {
	ctx    context.Context
	client *language.Client
}
