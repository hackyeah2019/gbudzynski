package handlers

import (
	"database/sql"
	"encoding/json"
	"gbudzynski/httpUtils"
	"net/http"
	"time"
)

func (t *TestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	httpUtils.AsJSON(w)
	defer httpUtils.CatchPanic(w)
	defer httpUtils.MeasureExecutionTime(r, time.Now())
	rows := make([]Row, 0)
	rowsCursor, err := t.DB.Query("SELECT key, value FROM gbudzynski.test")
	if err != nil {
		panic(err)
	}
	for rowsCursor.Next() {
		var row Row
		err := rowsCursor.Scan(&row.Key, &row.Value)
		if err != nil {
			panic(err)
		}
		rows = append(rows, row)
	}
	err = rowsCursor.Err()
	if err != nil {
		panic(err)
	}
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"hello": "world",
		"gb":    true,
		"rows":  rows,
		"path":  r.URL.Path,
		"query": r.URL.Query(),
	})
	if err != nil {
		panic(err)
	}
}

type TestHandler struct {
	DB *sql.DB
}

type Row struct {
	Key   string `json:"key"`
	Value int    `json:"value"`
}
