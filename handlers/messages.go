package handlers

import (
	"encoding/json"
	"gbudzynski/aws/comprehend"
	"gbudzynski/google/nlp"
	"gbudzynski/httpUtils"
	"gbudzynski/messageProcessor"
	"net/http"
	"time"
)

func (m *Messages) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer httpUtils.CatchPanic(w)
	defer httpUtils.MeasureExecutionTime(r, time.Now())
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	body := &messageProcessor.Message{}
	err := json.NewDecoder(r.Body).Decode(body)
	if err != nil {
		panic(err)
	}
	go messageProcessor.Process(
		m.ComprehendClient,
		m.NLPClient,
		body,
	)
	w.WriteHeader(http.StatusAccepted)
}

type Messages struct {
	ComprehendClient *comprehend.ComprehendClient
	NLPClient        *nlp.Client
}
