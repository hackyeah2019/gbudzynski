module gbudzynski

go 1.13

require (
	cloud.google.com/go v0.46.2
	github.com/araddon/dateparse v0.0.0-20190622164848-0fb0a474d195
	github.com/aws/aws-sdk-go v1.23.21
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/lib/pq v1.2.0
	github.com/pkg/errors v0.8.1 // indirect
	go.opencensus.io v0.22.1 // indirect
	golang.org/x/net v0.0.0-20190912160710-24e19bdeb0f2 // indirect
	golang.org/x/sys v0.0.0-20190913121621-c3b328c6e5a7 // indirect
	google.golang.org/api v0.10.0
	google.golang.org/appengine v1.6.2 // indirect
	google.golang.org/genproto v0.0.0-20190911173649-1774047e7e51
	google.golang.org/grpc v1.23.1 // indirect
)
