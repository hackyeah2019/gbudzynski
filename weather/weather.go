package weather

import (
	"encoding/json"
	"net/http"
	"os"
)

func MustGetWeather(city string) *WeatherResponse {
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		panic(err)
	}

	q := req.URL.Query()
	q.Set("q", city)
	q.Set("APPID", API_KEY)
	q.Set("units", "metric")
	req.URL.RawQuery = q.Encode()

	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	weather := &Weather{}
	err = json.NewDecoder(res.Body).Decode(weather)
	if err != nil {
		panic(err)
	}

	return &WeatherResponse{
		Description: weather.Data[0].Description,
		Icon:        weather.Data[0].Icon,
		Temp:        int(weather.Main.Temp),
		City:        weather.City,
	}
}

type Weather struct {
	Data []WeatherData `json:"weather"`
	Main WeatherMain   `json:"main"`
	City string        `json:"name"`
}

type WeatherData struct {
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

type WeatherMain struct {
	Temp float64 `json:"temp"`
}

type WeatherResponse struct {
	Description string `json:"description"`
	Icon        string `json:"icon"`
	Temp        int    `json:"temp"`
	City        string `json:"city"`
}

const (
	endpoint = "http://api.openweathermap.org/data/2.5/weather"
)

var (
	client  = &http.Client{}
	API_KEY = os.Getenv("WEATHER_API_KEY")
)
