package messageProcessor

import (
	"encoding/json"
	"fmt"
	comprehendTools "gbudzynski/aws/comprehend"
	"gbudzynski/google/nlp"
	"gbudzynski/leon"
	"gbudzynski/lot"
	. "gbudzynski/nlp"
	weatherPackage "gbudzynski/weather"
	"time"
)

func Process(comprehendClient *comprehendTools.ComprehendClient, nlpClient *nlp.Client, message *Message) {
	defer func(start time.Time) {
		fmt.Printf("processing took %s\n", time.Now().Sub(start))
		err := recover()
		if err != nil {
			fmt.Printf("%v\n", err)
			leon.PostMessage(&leon.Message{
				Type:      leon.Text,
				Status:    leon.Error,
				Text:      "Something went wrong. Is your input language supported?",
				To:        message.From,
				Timestamp: message.Timestamp,
			})
		}
	}(time.Now())

	fmt.Printf("<- %q\n", message.Text)

	errors := make(chan error)
	entitiesChan := make(chan []*Entity)

	go processAmazon(comprehendClient, nlpClient, entitiesChan, errors, message)
	go processGoogle(comprehendClient, nlpClient, entitiesChan, errors, message)

	entities := make([]*Entity, 0)

	for i := 0; i < 2; i++ {
		select {
		case err := <-errors:
			panic(err)
		case e := <-entitiesChan:
			for _, entity := range e {
				if !entityExists(entity, entities) {
					entities = append(entities, entity)
				}
			}
		}
	}

	weather := findEntityByText("weather", entities)
	date := findEntityByType("DATE", entities)
	location := findEntityByType("LOCATION", entities)
	other := findEntityByType("OTHER", entities)
	organization := findEntityByType("ORGANIZATION", entities)

	if weather != nil {
		w := weatherPackage.MustGetWeather(location.Text)
		serialized, err := json.Marshal(w)
		if err != nil {
			panic(err)
		}
		err = leon.PostMessage(&leon.Message{
			Type:      leon.Weather,
			Status:    leon.Success,
			Text:      string(serialized),
			To:        message.From,
			Timestamp: message.Timestamp,
		})
		if err != nil {
			panic(err)
		}
		return
	} else if date != nil {
		chosen := location
		if chosen == nil {
			chosen = other
		}
		if chosen == nil {
			chosen = organization
		}
		flights := lotClient.MustGetFlights(date.Text, chosen.Text)
		serialized, err := json.Marshal(flights)
		if err != nil {
			panic(err)
		}
		err = leon.PostMessage(&leon.Message{
			Type:      leon.Flights,
			Status:    leon.Success,
			Text:      string(serialized),
			To:        message.From,
			Timestamp: message.Timestamp,
		})
		if err != nil {
			panic(err)
		}
		return
	}

	text := message.Text
	debugText := ""

	for _, entity := range entities {
		debugText += fmt.Sprintf("%s\n", entity)
	}

	if len(entities) == 0 {
		text = "I didn't quite catch that. Please rephrase."
	} else {
		text = debugText
	}

	err := leon.PostMessage(&leon.Message{
		Type:      leon.Text,
		Status:    leon.Success,
		Text:      text,
		To:        message.From,
		Timestamp: message.Timestamp,
	})
	if err != nil {
		panic(err)
	}
}

func processAmazon(
	comprehendClient *comprehendTools.ComprehendClient,
	nlpClient *nlp.Client,
	entitiesChan chan []*Entity,
	errorsChan chan error,
	message *Message,
) {
	defer func(start time.Time) {
		fmt.Printf("AMAZON took %s\n", time.Now().Sub(start))
		err := recover()
		if err != nil {
			errorsChan <- err.(error)
		}
	}(time.Now())

	lang := comprehendClient.MustDetectLanguage(message.Text)
	entities := comprehendClient.MustDetectEntities(lang, message.Text)
	entitiesChan <- entities
}

func processGoogle(
	comprehendClient *comprehendTools.ComprehendClient,
	nlpClient *nlp.Client,
	entitiesChan chan []*Entity,
	errorsChan chan error,
	message *Message,
) {
	defer func(start time.Time) {
		fmt.Printf("GOOGLE took %s\n", time.Now().Sub(start))
		err := recover()
		if err != nil {
			errorsChan <- err.(error)
		}
	}(time.Now())
	entities := nlpClient.MustDetectEntities(message.Text)
	entitiesChan <- entities
}

func entityExists(entity *Entity, entities []*Entity) bool {
	for _, e := range entities {
		if e.Text == entity.Text {
			return true
		}
	}
	return false
}

func findEntityByText(filter string, entities []*Entity) *Entity {
	for _, e := range entities {
		if e.Text == filter {
			return e
		}
	}
	return nil
}

func findEntityByType(filter string, entities []*Entity) *Entity {
	for _, e := range entities {
		if e.Type == filter {
			return e
		}
	}
	return nil
}

type Message struct {
	Text      string `json:"text"`
	From      int    `json:"from"`
	Timestamp int    `json:"timestamp"`
}

var (
	lotClient = lot.MustNewClient()
)
