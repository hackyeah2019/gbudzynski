package db

import (
	"database/sql"
	_ "github.com/lib/pq"
	"os"
)

func MustNew() *sql.DB {
	db, err := sql.Open(DATABASE_TYPE, DATABASE_URL)
	if err != nil {
		panic(err)
	}
	return db
}

var (
	DATABASE_URL  = os.Getenv("DATABASE_URL")
	DATABASE_TYPE = "postgres"
)
